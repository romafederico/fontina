import React from 'react';
import { Switch, Route } from 'react-router-dom';
import UserList from './UserList'
import UserProfile from './UserProfile'

const User = () => (
    <div className="container">
        <Switch>
            <Route path="/user" component={UserList} />
            <Route path="/user/:id" component={UserProfile} />
        </Switch>
    </div>
);

export default User;