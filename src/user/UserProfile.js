import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loading from '../base/Loading';
import { fetchUser, fetchStory } from '../base/Utils';

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: null,
            stories:  null,
            isLoading: true,
        };
        this.setUser = this.setUser.bind(this);
        this.setStories = this.setStories.bind(this);
    }

    setUser(user) {
        this.setState({ user: user });
        this.setState({ isLoading: false });
    }

    setStories(stories) {
        this.setState({ stories: stories });
    }

    render() {
        const { isLoading, user, stories } = this.state;
        if(isLoading === true) {
            return ( <Loading/>)
        } else {
            if(user && user.status === 2) {
                return (
                    <div className="container">
                        <img src={"http://bukium.com/images/UsuariosWeb/IMG_" + user.user_id + "_150x150.jpg"} alt=""/>
                        <h1>{user.username}</h1>
                        <h3>{user.firstname} {user.lastname}</h3>
                        <p>{user.bio}</p>
                        <h3>Historias del autor</h3>
                        <ul>
                            {stories.map(story => <Link to={"/story/" + story.story_id}><li>{story.title}</li></Link>)}
                        </ul>
                    </div>
                )
            } else {
                return ( <h3>Este usuario no esta disponble</h3> )
            }
        }
    }

    componentDidMount() {
        fetchUser({'user_id': this.props.match.params.id})
            .then(user => this.setUser(user[0]))
            .catch(error => console.log(error));
        fetchStory({'user_id': this.props.match.params.id, 'status':2})
            .then(stories => this.setStories(stories))
            .catch(error => console.log(error));
    };
}

export default UserProfile;