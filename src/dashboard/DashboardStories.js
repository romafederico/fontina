import React, { Component } from 'react';
import DashboardNav from './DashboardNav'
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { fetchStory } from '../base/Utils';
import Loading from '../base/Loading';


class DashboardStories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stories: [],
        }

        this.setStories = this.setStories.bind(this);
    }

    setStories(stories) {
        this.setState({ stories: stories});
    }

    render() {
        const { isLoading, stories } = this.state;
        if(this.props.user.length === 0) {
            return (
                <Redirect to={"/login"}/>
            )
        }

        return (
            <div>
                <DashboardNav/>
                <h2>Tus Historias</h2>
                <h4><Link to={"/write/35"}>obra ajena</Link></h4>
                <ul>
                    {stories.map(story => <Link to={"/write/" + story.story_id}><li>{story.title}</li></Link>)}
                </ul>
            </div>
        )
    }


    componentDidMount() {
        fetchStory({'user_id': this.props.user.user_id})
            .then(stories => this.setStories(stories))
            .catch(error => console.log(error));
    }
};


function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

export default connect(mapStateToProps)(DashboardStories);