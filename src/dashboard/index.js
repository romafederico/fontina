import React from 'react';
import { Switch, Route, NavLink } from 'react-router-dom';

import DashboardProfile from './DashboardProfile'
import DashboardStories from './DashboardStories';
import DashboardPeople from './DashboardPeople';
import DashboardFavourites from './DashboardFavourites';
import DashboardStats from './DashboardStats';
import DashboardAchievements from './DashboardAchievements';

const Dashboard = () => (
    <div>
        <ul className="dashboard-nav">
            <li>
                <NavLink activeClassName='active' to="/profile">
                    Perfil
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName='active' to="/stories">
                    Historias
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName='active' to="/people">
                    Gente
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName='active' to="/favourites">
                    Favoritos
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName='active' to="/stats">
                    Estadíisticas
                </NavLink>
            </li>
            <li>
                <NavLink activeClassName='active' to="/profile">
                    Logros
                </NavLink>
            </li>
        </ul>
        <Switch>
            <Route path='/profile' component={DashboardProfile} />
            <Route path='/stories' component={DashboardStories} />
            <Route path='/people' component={DashboardPeople} />
            <Route path='/favourites' component={DashboardFavourites} />
            <Route path='/stats' component={DashboardStats} />
            <Route path='/achievements' component={DashboardAchievements} />
        </Switch>
    </div>
);

export default Dashboard;