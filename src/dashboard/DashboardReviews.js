import React, { Component } from 'react';
import DashboardNav from './DashboardNav';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';


class DashboardReviews extends Component {

    render() {
        if(this.props.user.length === 0) {
            return (
                <Redirect to={"/login"}/>
            )
        }

        return (
            <div>
                <DashboardNav/>
                Reviews
            </div>
        )
    }
};


function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

export default connect(mapStateToProps)(DashboardReviews);