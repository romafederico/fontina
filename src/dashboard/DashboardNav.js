import React from 'react';
import { NavLink } from 'react-router-dom'

const DashboardNav = () => (
    <div>
        <NavLink activeClassName='active' to="/dashboard/stories">Historias</NavLink>
        <NavLink activeClassName='active' to="/dashboard/reviews">Reseñas</NavLink>
        <NavLink activeClassName='active' to="/dashboard/profile">Perfil</NavLink>
    </div>
);

export default DashboardNav;