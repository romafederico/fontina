import React, { Component } from 'react'
import { Navbar, NavbarBrand, NavbarToggler, Nav, NavItem, Collapse } from 'reactstrap'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import login from '../actions/login';
import logout from '../actions/logout';
import FacebookLogin from 'react-facebook-login';
import { withRouter } from 'react-router-dom';


class Header extends Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
                isOpen: false
        };

        this.responseFacebook = this.responseFacebook.bind(this);
        this.logout = this.logout.bind(this);
    }

    responseFacebook(response) {
        this.props.login(response);
    }

    logout() {
        this.props.logout();
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div>
                <Navbar color="faded" light toggleable>
                    <NavbarToggler right onClick={this.toggle} />
                    <NavbarBrand><Link to={"/"}>bukium</Link></NavbarBrand>
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            {this.props.user.length !== 0 ? <NavItem><Link to={"/typer"}>Nueva Historia</Link></NavItem> : <span></span>}
                            {this.props.user.length !== 0 ? <NavItem><Link to={"/dashboard/stories"}><img src={this.props.user.facebook_avatar} alt=""/></Link></NavItem> : <span></span>}
                            {this.props.user.length !== 0 ? <NavItem><button onClick={this.logout}>Logout</button></NavItem> :
                                this.props.location.pathname === '/login' ? null :
                                <NavItem>
                                    <FacebookLogin
                                        appId="10154156606786566"
                                        autoLoad={true}
                                        fields="id,name,picture,first_name,last_name,email,birthday,updated_time,languages,locale,bio,hometown"
                                        scope="public_profile,user_friends,user_actions.books,email"
                                        callback={this.responseFacebook}
                                        size="small"
                                        version="2.9"
                                    />
                                </NavItem>}
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login: login,
        logout: logout,
    }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
