const ip = require('ip');

function buildQueryString(obj) {
  let str = [];
  for(let p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

function fetchUser(query) {
    let querystring = buildQueryString(query);
    return fetch(`http://localhost:8000/api/users?${querystring}`)
        .then(response => response.json())
        .catch(error => console.log(error.message));
}

function fetchStory(query) {
    let querystring = buildQueryString(query);
    return fetch(`http://localhost:8000/api/stories?${querystring}`)
        .then(response => response.json())
        .catch(error => console.log(error.message));
}

function fetchStoryLatest() {
    return fetch(`http://localhost:8000/api/stories/latest`)
        .then(response => response.json())
        .catch(error => console.log(error.message));
}

function fetchChapters(query) {
    let querystring = buildQueryString(query);
    return fetch(`http://localhost:8000/api/chapters?${querystring}`)
        .then(response => response.json())
        .catch(error => console.log(error.message));
}

function createUser(user) {
    return fetch(`http://localhost:8000/api/users`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(user),
    })
        .then(response => response.json())
        .catch(error => console.log(error));
}

function updateUser(user, data) {
    return fetch(`http://localhost:8000/api/users/${user}/`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(data),
    })
        .then(response => response.json())
        .catch(error => console.log(error));
}

function logLogin(id) {
    const headers = {
        "Content-Type": "application/json",
    };
    const data = {
        "user_id": id,
        "ip": ip.address(),
        "source": 'fb',
    };
    return fetch(`http://localhost:8000/api/stats/logins/`, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data),
    })
}

function logSignup(id) {
    const headers = {
        "Content-Type": "application/json",
    };
    const data = {
        "user_id": id,
        "ip": ip.address(),
        "source": 'fb',
    };
    return fetch(`http://localhost:8000/api/stats/signups/`, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data),
    })
}

export {
    fetchUser,
    fetchStory,
    fetchStoryLatest,
    fetchChapters,
    createUser,
    updateUser,
    logLogin,
    logSignup,
};