import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FacebookLogin from 'react-facebook-login';
import login from '../actions/login';

class Login extends Component {
    constructor(props) {
        super(props);
        this.responseFacebook = this.responseFacebook.bind(this);
    }

    responseFacebook(response) {
        this.props.login(response);
    }

    render() {
        if(this.props.user.length !== 0) {
            this.props.history.goBack();
        }
        return (
            <div>
                <FacebookLogin
                    appId="10154156606786566"
                    autoLoad={false}
                    fields="id,name,picture,first_name,last_name,email,birthday,updated_time,languages,locale,bio,hometown"
                    scope="public_profile,user_friends,user_actions.books,email"
                    callback={this.responseFacebook}
                    size="small"
                    version="2.9"
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login: login,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
