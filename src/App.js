import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom'

import './App.css';
import Header from './base/Header';
import Home from './home/Home';
import UserProfile from './user/UserProfile';
import StoryProfile from './story/StoryProfile';
import StoryContent from './story/StoryContent';
import Typer from './typer/Typer';
import Login from './base/Login';
import DashboardStories from './dashboard/DashboardStories';
import DashboardReviews from './dashboard/DashboardReviews';
import DashboardProfile from './dashboard/DashboardProfile';

class App extends Component {
    render() {
        return (
            <div className="App">
                {this.props.location.pathname.indexOf('/write') ? <Header/> : null }
                <Route exact path="/" component={Home} />
                <Route exact path="/user/:id" component={UserProfile} />
                <Route exact path="/story/:id" component={StoryProfile} />
                <Route exact path="/read/:id" component={StoryContent} />
                <Route exact path="/dashboard/stories" component={DashboardStories}/>
                <Route exact path="/dashboard/reviews" component={DashboardReviews}/>
                <Route exact path="/dashboard/profile" component={DashboardProfile}/>
                <Route path="/write/:id" component={Typer} />
                <Route exact path="/login" component={Login} />
            </div>
        );
    }
}

export default withRouter(App);
