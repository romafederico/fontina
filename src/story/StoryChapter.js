import React from 'react';

const StoryChapter = ({ content }) =>
    <div>{content}</div>

export default StoryChapter;