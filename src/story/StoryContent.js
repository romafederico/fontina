import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Loading from '../base/Loading';
import { fetchStory, fetchChapters } from '../base/Utils';
import StoryChapter from './StoryChapter';

import { connect } from 'react-redux'

class StoryContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            story: null,
            content: null,
            currentChapter: 0,
            isLoading:true,
        };
        this.setStory = this.setStory.bind(this);
        this.setContent = this.setContent.bind(this);
        this.upChapter = this.upChapter.bind(this);
        this.downChapter = this.downChapter.bind(this);
    }

    setStory(story) {
        this.setState({ story: story });
        this.setState({ isLoading: false });
    }

    setContent(content) {
        this.setState({ content: content });
    }

    upChapter() {
        this.setState({ currentChapter: this.state.currentChapter + 1});
    }

    downChapter() {
        this.setState({ currentChapter:  this.state.currentChapter - 1})
    }

    render() {

        const { isLoading, story, content, currentChapter } = this.state;

        if(this.props.user.length === 0) {
            return (
                <Redirect to={"/login"}/>
            )
        }

        if(isLoading === true) {
            return ( <Loading/> );
        } else {
            if(story && story.status === 2) {
                return (
                    <div className="container">
                        <p><Link to={"/story/" + story.story_id}>Volver</Link></p>
                        <p>{story.title}</p>
                        <h1>{content[currentChapter].title}</h1>
                        <StoryChapter content={content[currentChapter].content} />
                        { currentChapter > 0 ? <button className="btn btn-primary" onClick={this.downChapter}>Anterior</button> : <span></span>}
                        { currentChapter < content.length - 1 ? <button className="btn btn-primary" onClick={this.upChapter}>Siguiente</button> : <p>Fin</p> }
                    </div>
                )
            } else {
                return ( <h3>Esta historia no esta disponible</h3>)
            }
        }
    }

    componentDidMount() {
        fetchStory({'story_id': this.props.match.params.id, 'status': 2})
            .then(story => this.setStory(story[0]))
            .catch(error => console.log(error));
        fetchChapters({'story_id': this.props.match.params.id})
            .then(content => this.setContent(content))
            .catch(error => console.log(error));
    };
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

export default connect(mapStateToProps)(StoryContent);