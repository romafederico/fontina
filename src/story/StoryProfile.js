import React, { Component } from 'react';
import Loading from '../base/Loading';
import { Link } from 'react-router-dom';
import { fetchStory } from '../base/Utils';

import {connect} from 'react-redux';

class StoryProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            story: null,
            isLoading:true,
        };
        this.setStory = this.setStory.bind(this);
    }

    setStory(story) {
        this.setState({ story: story });
        this.setState({ isLoading: false });
    }

    render() {
        const { isLoading, story } = this.state;
        if(isLoading === true) {
            return ( <Loading/> );
        } else {
            if(story && story.status === 2) {
                return (
                    <div className="container">
                        <img src={"http://bukium.com/images/Contenidos/IMG_" + story.story_id + "_180x250.jpg"} alt=""/>
                        <h1>{story.title}</h1>
                        <h5>por <Link to={"/user/" + story.user_id}>{story.username}</Link></h5>
                        <Link className="btn btn-primary lg" to={"/read/" + story.story_id}>Leer</Link>
                        <h3>{story.summary}</h3>
                    </div>
                )
            } else {
                return ( <h3>Esta historia no esta disponible</h3>)
            }
        }
    }

    componentDidMount() {
        fetchStory({'story_id': this.props.match.params.id})
            .then(story => this.setStory(story[0]))
            .catch(error => console.log(error));
    };
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

export default connect(mapStateToProps)(StoryProfile);