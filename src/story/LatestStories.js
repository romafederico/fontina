import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Loading from '../base/Loading'
import { fetchStoryLatest } from '../base/Utils';

class LatestStories extends Component {
    constructor(props){
        super(props)
        this.state = {
            stories: [],
            isLoading: true,
        }

        this.setStories = this.setStories.bind(this);
    }

    setStories(stories) {
        this.setState({ stories: stories, isLoading: false })
    }

    render() {
        const { isLoading, stories } = this.state;
        if(isLoading) {
            return ( <Loading/> )
        } else {
            return (
                <div className=".col .col-sm-12 .col-md-6 .col-md-offset-3">
                    <ul>
                        {stories.map(story =>
                            <li>
                                <Link to={"/story/" + story.story_id}>
                                    <img src={"http://bukium.com/images/Contenidos/IMG_" + story.story_id + "_180x250.jpg"} alt=""/>
                                    <h3>{story.title}</h3>
                                </Link>
                            </li>
                        )}
                    </ul>
                </div>
            )
        }
    }

    componentDidMount() {
        fetchStoryLatest()
            .then(stories => this.setStories(stories))
            .catch(error => console.log(error))
    }
}

export default LatestStories;