import React from 'react';
import { Switch, Route } from 'react-router-dom';
import StoryProfile from './StoryProfile'

const Story = () => (
    <div className="container">
        <Switch>
            <Route path="/story/:id" component={StoryProfile} />
        </Switch>
    </div>
);

export default Story;