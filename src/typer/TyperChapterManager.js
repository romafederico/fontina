import React, { Component } from 'react';

class TyperChapterManager extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let chapters = this.props;
        console.log(chapters);
        return(
            <div>
                <ul>
                Typer Chapter Manager
                    {chapters.map(chapter => <li><span>{chapter.chapter_number}</span> - <span>{chapter.title}</span></li>)}
                </ul>
            </div>
        )
    }
};

export default TyperChapterManager;