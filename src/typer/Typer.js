import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Loading from '../base/Loading';
import { fetchStory, fetchChapters } from '../base/Utils';
import TyperChapterEditor from './TyperChapterEditor';
import TyperChapterManager from './TyperChapterManager';

import { connect } from 'react-redux'

class Typer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            story: [],
            content: [],
            chapters: [],
            currentChapter: 0,
            isLoading:true,
            isChapterManagerOpen: false,
        };
        this.setStory = this.setStory.bind(this);
        this.setContent = this.setContent.bind(this);
        this.upChapter = this.upChapter.bind(this);
        this.downChapter = this.downChapter.bind(this);
        this.toggleChapterManager = this.toggleChapterManager.bind(this);
    }

    setStory(story) {
        this.setState({ story: story });
        this.setState({ isLoading: false });
    }

    setContent(content) {
        let chapters = [];
        content.map(chapter => {
            let obj = {
                chapter_numner: chapter.chapter_number,
                title: chapter.title,
            };
            chapters.push(obj);
        });
        this.setState({ content: content, chapters: chapters });
    }

    upChapter() {
        this.setState({ currentChapter: this.state.currentChapter + 1});
    }

    downChapter() {
        this.setState({ currentChapter:  this.state.currentChapter - 1})
    }

    toggleChapterManager() {
        this.setState({
            isChapterManagerOpen: !this.state.isChapterManagerOpen
        });
    }

    render() {

        const { isLoading, isChapterManagerOpen, story, content, chapters, currentChapter } = this.state;

        if(this.props.user.length === 0) {
            return (
                <Redirect to={"/login"}/>
            )
        }

        if(isLoading === true) {
            return ( <Loading/> );
        } else if (this.props.user.user_id !== story.user_id) {
            return (
                <div>
                    <p><Link to={"/dashboard/stories"}>Volver</Link></p>
                    <h3>Esta obra no te pertenece</h3>
                </div>)
        } else {
            return (
                <div className="container">
                    {
                        isChapterManagerOpen ?
                        <div onClick={this.toggleChapterManager} style={{position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, background: 'rgba(0, 0, 0, 0.15)'}}>
                            <div style={{position: 'absolute', background: '#fff', top: 25, left: '10%', right: '10%', padding: 15}}>
                                <TyperChapterManager chapters={chapters}/>
                            </div>
                        </div> : null
                    }
                    <p><Link to={"/dashboard/stories"}>Volver</Link></p>
                    <p onClick={this.toggleChapterManager}>Capitulos</p>
                    <p>{story.title}</p>
                    <h1>{content[currentChapter].title}</h1>
                    <TyperChapterEditor content={content[currentChapter].content}/>
                    { currentChapter > 0 ?
                        <button className="btn btn-primary" onClick={this.downChapter}>Anterior</button> :
                        <span></span>}
                    { currentChapter < content.length - 1 ?
                        <button className="btn btn-primary" onClick={this.upChapter}>Siguiente</button> : <p>Fin</p> }
                </div>
            )
        }
    }

    componentDidMount() {
        fetchStory({'story_id': this.props.match.params.id, 'status': 2})
            .then(story => this.setStory(story[0]))
            .catch(error => console.log(error));
        fetchChapters({'story_id': this.props.match.params.id})
            .then(content => this.setContent(content))
            .catch(error => console.log(error));
    };
}

function mapStateToProps(state) {
    return {
        user: state.activeUser
    }
}

export default connect(mapStateToProps)(Typer);