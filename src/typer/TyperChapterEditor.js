import React from 'react';

const TyperChapterEditor = ({ content }) => (
    <div contentEditable>
        {content}
    </div>
);

export default TyperChapterEditor;