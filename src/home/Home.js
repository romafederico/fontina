import React from 'react';
import LatestStories from '../story/LatestStories';

const Home = () => (
    <div>
        <LatestStories/>
    </div>
);

export default Home;