import { LOG_IN } from './types';
import { fetchUser, createUser, updateUser, logLogin, logSignup } from '../base/Utils'

export default function login(response) {
    return dispatch => {
        fetchUser({'email': response.email})
            .then(user => {
                if(user[0]) {
                    dispatch(getUserAsync(user[0]));
                    let email = {
                        "facebook_id": response.id,
                    };
                    updateUser(user[0].user_id, email);
                    logLogin(user[0].user_id);
                } else {
                    fetchUser({'facebook_id': response.id})
                        .then(user => {
                            if(user[0]) {
                                dispatch(getUserAsync(user[0]));
                                let data = {
                                    "email": response.email,
                                };
                                updateUser(user[0].user_id, data);
                                logLogin(user[0].user_id);
                            } else {
                                user = {
                                    "firstname": response.first_name,
                                    "lastname": response.last_name,
                                    "email": response.email,
                                    "password": 'NO',
                                    'facebook_id': response.id,
                                    "facebook_avatar": response.picture.data.url,
                                    "status": 2,
                                    "credits": 0,
                                };
                                createUser(user)
                                    .then(user => {
                                        dispatch(getUserAsync(user));
                                        logSignup(user.user_id);
                                    })
                            }
                        })
                }
            })
            .catch(error => console.log(error));
    }
};

function getUserAsync(people){
  return {
    type: LOG_IN,
    payload: people
  }
}
