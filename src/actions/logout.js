import { LOG_OUT } from './types';

export default function logout() {
    return logoutAsync();
};

function logoutAsync() {
    return {
        type: LOG_OUT,
        payload: [],
    }
};
