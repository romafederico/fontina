import { LOG_IN } from '../actions/types';
import { LOG_OUT } from '../actions/types';

export default function(state=[], action) {

  switch (action.type) {
    case LOG_IN:
      return action.payload;

    case LOG_OUT:
      return action.payload;

    default:
      return state;
  }

}