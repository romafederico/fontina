import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import activeUserReducer from './activeUserReducer';

const rootReducers = combineReducers({
    router: routerReducer,
    activeUser: activeUserReducer,
});

export default rootReducers;
